package com.gnomon.epsos.rest;

import com.gnomon.epsos.model.Country;
import com.gnomon.epsos.model.Identifier;
import com.gnomon.epsos.model.Patient;
import com.gnomon.epsos.model.PatientDocument;
import com.gnomon.epsos.model.UserData;
import com.gnomon.epsos.model.adapter.PatientAdapter;
import com.gnomon.epsos.model.adapter.PatientDocumentAdapter;
import com.gnomon.epsos.model.queries.PatientDiscovery;
import com.gnomon.epsos.service.Demographics;
import com.gnomon.epsos.service.EpsosHelperService;
import com.gnomon.epsos.service.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CompanyConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import epsos.openncp.protocolterminator.clientconnector.PatientDemographics;
import epsos.openncp.protocolterminator.clientconnector.PatientId;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.opensaml.saml2.core.Assertion;
import org.w3c.dom.Document;

/**
 *
 * @author karkaletsis
 *
 */
@Path("/")
public class EpsosRestService {

    private static final int bypassRefererChecking = 1;
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger("EpsosRestService");
    private static final long companyId = 10157;
    private static final Utils utils = new Utils();

    @GET
    @Path("/{param}")
    public Response getMsg(@Context HttpServletRequest request, @PathParam("param") String msg) {
        String ip = request.getRemoteAddr();
        log.info("IP ADDRESS IS : " + ip);
        String output = "OpenNCP says : " + msg;
        return Response.status(200).entity(output).build();
    }

    @POST
    @Path("/users/login")
    public String userLogin(
            @FormParam("username") String username,
            @FormParam("password") String password,
            @HeaderParam("referer") String referer) throws PortalException, SystemException, UnsupportedEncodingException {
        if (!validReferer(referer)) {
            return "";
        }
        log.info("user: " + username);
        log.info("password: " + password);
        username = URLDecoder.decode(username, "UTF-8");
        log.info("companyId: " + companyId);
        int usertype = 100;
        User user = null;
        String ret = "";
        try {
            log.info("Try to find user with screenname :" + username);
            user = UserLocalServiceUtil.getUserByScreenName(companyId, username);
        } catch (Exception e) {
            log.error("Error find user by screenname " + username);
        }
        if (Validator.isNull(user)) {
            try {
                log.info("Try to find user with email :" + username);
                user = UserLocalServiceUtil.getUserByEmailAddress(companyId, username);
            } catch (Exception e) {
                log.error("Error find user by emailaddress " + username);
            }
        }

        long auth = 0;
        if (Validator.isNotNull(user)) {

            if (!user.isActive()) {
                log.info("USER ACTIVE: " + user.isActive());
                ret = "401";
            } else {

                auth = UserLocalServiceUtil.authenticateForBasic(companyId,
                        CompanyConstants.AUTH_TYPE_EA,
                        username, password);
                if (auth < 1) {
                    auth = UserLocalServiceUtil.authenticateForBasic(companyId,
                            CompanyConstants.AUTH_TYPE_SN,
                            username, password);
                }
                log.info("auth: " + auth);
                if (auth == 0) {
                    ret = "400";
                }
            }
        }
        log.info("# AUTH " + auth);
        UserData ud = new UserData();
        if (auth > 0) {
            ud.setStatus("1");
            ud.setRet("200");
        } else {
            ud.setStatus("0");
            ud.setRet("400");
            ud.setEmailaddress("");
            ud.setUserId("0");
        }
        ud.setRet(ret);
        try {
            String encauth = Utils.encrypt(auth + "");
            log.info("### : " + encauth);
            if (auth > 0) {
                ud.setRet("200");
                ud.setUserId(encauth);
                ud.setScreenname(user.getScreenName());
                ud.setEmailaddress(user.getEmailAddress());
                ud.setUsertype(usertype);
            }
        } catch (Exception ex) {
            log.error("Error encrypring");
        }

        Gson gson = new Gson();
        ret = gson.toJson(ud);
        return ret;
    }

    @GET
    @Path("/get/countries/names/{username}/{language}")
    public String getCountries(
            @Context HttpServletRequest request,
            @PathParam("username") String username,
            @PathParam("language") String language,
            @HeaderParam("referer") String referer) throws PortalException, SystemException, Exception {
        log.info("Get Countries for language: " + language);
        log.info("DECODED username : " + username);
        String ret = "";
        if (!utils.isValidUser(username)) {
            return "";
        }
        String path = request.getRealPath("/");
        log.info("Application Path is: " + path);
        List<Country> countries = EpsosHelperService.getCountriesFromCS(language, path);
        Gson gson = new Gson();
        ret = gson.toJson(countries);
        return ret;
    }

    @GET
    @Path("/get/countries/attributes/{username}/{country}/{language}")
    public String getCountryAttributes(
            @Context HttpServletRequest request,
            @PathParam("username") String username,
            @PathParam("country") String country,
            @PathParam("language") String language,
            @HeaderParam("referer") String referer) throws PortalException, SystemException, Exception {
        log.info("Get attributes for language: " + country);
        log.info("DECODED username : " + username);
        String ret = "";
        if (!utils.isValidUser(username)) {
            return "";
        }
        String path = request.getRealPath("/") + "/WEB-INF/";
        log.info("#################: " + path);
        Vector countryIdentifiers = EpsosHelperService.getCountryIdsFromCS(country, path);
        List<Identifier> identifiers = EpsosHelperService.getCountryIdentifiers(country, language, path, null);

        Vector countryDemographics = EpsosHelperService.getCountryDemographicsFromCS(country, path);
        List<Demographics> demographics = EpsosHelperService.getCountryDemographics(country, language, path, null);

        HashMap hp = new HashMap();
        hp.put("identifiers", identifiers);
        hp.put("demographics", demographics);

        Gson gson = new Gson();
        ret = gson.toJson(hp);
        return ret;
    }

    /*
     <PatientDiscovery>
     <identifiers>
     <identifier root="1.3.6.1.4.1.26580.10" extension="551121234" />
     </identifiers>
     <demographics>
     <firstname>Martha</firstname>
     <lastname>Token</lastname>
     <country>KP</country>
     </demographics>
     </PatientDiscovery>
     */
    @POST
    @Path("/search/patients/{username}")
    public String searchPatients(
            StreamSource streamSource,
            @PathParam("username") String username) throws Exception {
        log.info("Get into search patients");
        String ret = "";
        String soapMessage = "";
        if (!utils.isValidUser(username)) {
            return "";
        }
        User user = utils.getLiferayUser(username);
        Object ass = EpsosHelperService.getUserAssertion(user);
        log.info("The assertion for user has been created: " + ((Assertion) ass).getID());
        try {
            Document doc = Utils.readXml(streamSource);
            soapMessage = Utils.transformDomToString(doc);
        } catch (Exception e) {
            log.error("Error processing request");
            log.error(ExceptionUtils.getStackTrace(e));
        }

        JAXBContext jaxbContext;
        try {
            jaxbContext = JAXBContext.newInstance(PatientDiscovery.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            StringReader reader = new StringReader(soapMessage);
            PatientDiscovery pdq = (PatientDiscovery) jaxbUnmarshaller.unmarshal(reader);
            PatientDemographics pd = PatientDemographics.Factory.newInstance();

            int pisize = pdq.getIdentifiers().getIdentifier().size();
            PatientId[] idArray = new PatientId[pisize];
            for (int i = 0; i < pisize; i++) {
                PatientId id = PatientId.Factory.newInstance();
                id.setRoot(pdq.getIdentifiers().getIdentifier().get(i).getRoot());
                id.setExtension(pdq.getIdentifiers().getIdentifier().get(i).getExtension());
                idArray[i] = id;
            }

            pd.setFamilyName(pdq.getDemographics().getLastname());
            pd.setGivenName(pdq.getDemographics().getFirstname());
            //pd.setCountry(pdq.getDemographics().getCountry());
            pd.setCity(pdq.getDemographics().getCity());
            pd.setPostalCode(pdq.getDemographics().getPostalCode());
            pd.setAdministrativeGender(pdq.getDemographics().getGender());
            if (Validator.isNotNull(pd.getBirthDate())) {
                try {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(pdq.getDemographics().getBirthDate());
                    pd.setBirthDate(cal);
                } catch (Exception ex) {
                    log.error(ex.getMessage());
                }
            }

            pd.setPatientIdArray(idArray);
            List<Patient> patients = EpsosHelperService.searchPatients((Assertion) ass, pd, pdq.getDemographics().getCountry());
            log.info("Patients found: " + patients.size());

            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.registerTypeAdapter(Patient.class,
                    new PatientAdapter()).create();
            ret = gson.toJson(patients);
        } catch (JAXBException ex) {
            log.error(ExceptionUtils.getStackTrace(ex));
        }
        return ret;
    }

    @GET
    @Path("/search/documents/ps/{username}/{country}/{root}/{extension}")
    public String getPatientPSDocuments(
            @PathParam("username") String username,
            @PathParam("country") String country,
            @PathParam("root") String root,
            @PathParam("extension") String extension) throws Exception {

        log.info("Get into search patients");
        String ret = "";
        String soapMessage = "";
        if (!utils.isValidUser(username)) {
            return "";
        }
        User user = utils.getLiferayUser(username);
        Object ass = EpsosHelperService.getUserAssertion(user);
        log.info("The assertion for user has been created: " + ((Assertion) ass).getID());
        PatientId patientId = PatientId.Factory.newInstance();
        patientId.setExtension(extension);
        patientId.setRoot(root);
        String purposeOfUse = "TREATMENT";
        log.info("TRCA: Creating trca for hcpAssertion : " + ((Assertion) ass).getID() + " for patient " + patientId.getRoot() + ". Purpose of use is : " + purposeOfUse);
        Object trcAssertion = EpsosHelperService.createPatientConfirmationPlain(purposeOfUse, (Assertion) ass, patientId);
        log.info("TRCA: Created " + ((Assertion) trcAssertion).getID() + " for : " + ((Assertion) ass).getID() + " for patient " + patientId.getRoot() + "_" + patientId.getExtension() + ". Purpose of use is : " + purposeOfUse);
        List<PatientDocument> patientDocuments = EpsosHelperService.getPSDocs(
                (Assertion) ass,
                (Assertion) trcAssertion,
                root,
                extension,
                country);
        log.info("PS Docs found: " + patientDocuments.size());

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.registerTypeAdapter(PatientDocument.class,
                new PatientDocumentAdapter()).create();
        ret = gson.toJson(patientDocuments);
        return ret;
    }

    @GET
    @Path("/search/documents/ep/{username}/{country}/{patientid}")
    public String getPatientEPDocuments(@PathParam("username") String username,
            @PathParam("country") String country,
            @PathParam("root") String root,
            @PathParam("extension") String extension) throws Exception {
        log.info("Get into search patients");
        String ret = "";
        String soapMessage = "";
        if (!utils.isValidUser(username)) {
            return "";
        }
        User user = utils.getLiferayUser(username);
        Object ass = EpsosHelperService.getUserAssertion(user);
        log.info("The assertion for user has been created: " + ((Assertion) ass).getID());
        PatientId patientId = PatientId.Factory.newInstance();
        patientId.setExtension(extension);
        patientId.setRoot(root);
        String purposeOfUse = "TREATMENT";
        log.info("TRCA: Creating trca for hcpAssertion : " + ((Assertion) ass).getID() + " for patient " + patientId.getRoot() + ". Purpose of use is : " + purposeOfUse);
        Object trcAssertion = EpsosHelperService.createPatientConfirmationPlain(purposeOfUse, (Assertion) ass, patientId);
        log.info("TRCA: Created " + ((Assertion) trcAssertion).getID() + " for : " + ((Assertion) ass).getID() + " for patient " + patientId.getRoot() + "_" + patientId.getExtension() + ". Purpose of use is : " + purposeOfUse);
        List<PatientDocument> patientDocuments = EpsosHelperService.getPSDocs(
                (Assertion) ass,
                (Assertion) trcAssertion,
                root,
                extension,
                country);
        log.info("PS Docs found: " + patientDocuments.size());

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.registerTypeAdapter(PatientDocument.class,
                new PatientDocumentAdapter()).create();
        ret = gson.toJson(patientDocuments);
        return ret;
    }

    @GET
    @Path("/search/mydocuments/ps/{username}")
    public String getMyPatientPSDocuments() {
        return "";
    }

    @GET
    @Path("/search/mydocuments/ep/{username}")
    public String getMyPatientEPDocuments() {
        return "";
    }

    @GET
    @Path("/get/document/{username}/{country}/{root}/{extension}/{repositoryid}/{homecommunityid}/{documentid}/{doctype}")
    public String retrieveDocument(
            @PathParam("username") String username,
            @PathParam("country") String country,
            @PathParam("root") String root,
            @PathParam("extension") String extension,
            @PathParam("repositoryid") String repositoryid,
            @PathParam("homecommunityid") String homecommunityid,
            @PathParam("documentid") String documentid,
            @PathParam("doctype") String doctype
    ) throws Exception {
        log.info("Get into retrieve document");
        String ret = "";
        String soapMessage = "";
        if (!utils.isValidUser(username)) {
            return "";
        }
        User user = utils.getLiferayUser(username);
        Object ass = EpsosHelperService.getUserAssertion(user);
        log.info("The assertion for user has been created: " + ((Assertion) ass).getID());
        PatientId patientId = PatientId.Factory.newInstance();
        patientId.setExtension(extension);
        patientId.setRoot(root);
        String purposeOfUse = "TREATMENT";
        log.info("TRCA: Creating trca for hcpAssertion : " + ((Assertion) ass).getID() + " for patient " + patientId.getRoot() + ". Purpose of use is : " + purposeOfUse);
        Object trcAssertion = EpsosHelperService.createPatientConfirmationPlain(purposeOfUse, (Assertion) ass, patientId);
        log.info("TRCA: Created " + ((Assertion) trcAssertion).getID() + " for : " + ((Assertion) ass).getID() + " for patient " + patientId.getRoot() + "_" + patientId.getExtension() + ". Purpose of use is : " + purposeOfUse);
        String cda = EpsosHelperService.getDocument((Assertion) ass, (Assertion) trcAssertion, country, root, extension, repositoryid, homecommunityid, documentid, doctype, "el-GR");
        return cda;
    }

    @POST
    @Path("/submit/document/ed/{username}/{country}")
    public String submitEDDocument() {
        return "";
    }

    @POST
    @Path("/submit/document/hcer/{username}/{country}")
    public String submitHCERDocument() {
        return "";
    }

    private boolean validReferer(String referer) {
        log.info("##### bypassRefererChecking " + bypassRefererChecking);
        if (bypassRefererChecking == 1) {
            return true;
        }
        if (Validator.isNull(referer)) {
            return false;
        }
        return referer.contains("gnomon");
    }

}
