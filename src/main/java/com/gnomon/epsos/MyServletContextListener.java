package com.gnomon.epsos;

import com.gnomon.epsos.hcer.HcerPersistentManager;
import static com.gnomon.epsos.hcercontrol.HCERControl.logger;
import com.gnomon.epsos.service.EpsosHelperService;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import epsos.ccd.posam.tm.service.ITransformationService;
import epsos.openncp.protocolterminator.ClientConnectorConsumer;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import org.apache.log4j.Logger;
import org.hibernate.exception.ExceptionUtils;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import tr.com.srdc.epsos.util.Constants;

@WebListener
public class MyServletContextListener implements ServletContextListener {

    private static final Logger log = Logger.getLogger("MyServletContextListener");
    private ServletContext context = null;
    private static String runningMode;
    private static ITransformationService tService;
    private static ClientConnectorConsumer clientConectorConsumer;

    // Public constructor is required by servlet spec
    public MyServletContextListener() {
    }

    // -------------------------------------------------------
    // ServletContextListener implementation
    // -------------------------------------------------------
    public void contextInitialized(ServletContextEvent sce) {
        log.info("Context Listener > Initialized");
        try {
            runningMode = GetterUtil.get(PropsUtil.get("running.mode"), "live");
        } catch (Exception e) {
            runningMode = "live";
            log.error(ExceptionUtils.getStackTrace(e));
        }

        log.info("Initiating OpenNCP Portal");
        try {
            System.setProperty("javax.net.ssl.keyStore", Constants.NCP_SIG_KEYSTORE_PATH);
            System.setProperty("javax.net.ssl.keyStorePassword", Constants.NCP_SIG_KEYSTORE_PASSWORD);
            System.setProperty("javax.net.ssl.key.alias", Constants.NCP_SIG_PRIVATEKEY_ALIAS);
            System.setProperty("javax.net.ssl.privateKeyPassword", Constants.NCP_SIG_PRIVATEKEY_PASSWORD);
            System.setProperty("javax.net.ssl.trustStore", Constants.TRUSTSTORE_PATH);
            System.setProperty("javax.net.ssl.trustStorePassword", Constants.TRUSTSTORE_PASSWORD);
        } catch (Exception e) {
            log.error("#### ERROR INITIALIZING KEYSTORE/TRUSTSTORE ####");
        }

        log.info("Initializing TM component");
        try {
            ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("ctx_tm.xml");
            tService = (ITransformationService) applicationContext.getBean(ITransformationService.class.getName());
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e));
            log.error("#### ERROR INITIALIZING TM ####");
        }
        try {
            String serviceUrl = EpsosHelperService.getConfigProperty(EpsosHelperService.PORTAL_CLIENT_CONNECTOR_URL);			//serviceUrl = LiferayUtils.getFromPrefs("client_connector_url");
            log.info("SERVICE URL IS " + serviceUrl);
            clientConectorConsumer = new ClientConnectorConsumer(serviceUrl);
        } catch (Exception e) {
            log.error("ERROR INITIALIZING CLIENT CONNECTOR PROXY");
        }

        log.info("Running Mode: " + runningMode);

        // hcer init
        InputStream input = null;
        try {
            String hcerConfigPath = System.getenv("EPSOS_PROPS_PATH") + "hcer.properties";
            logger.info("CONFIG PATH: " + hcerConfigPath);
            Properties prop = new Properties();
            String dialect = "";
            input = new FileInputStream(System.getenv("EPSOS_PROPS_PATH") + "hcer.properties");
            prop.load(input);
            String driverClassName = prop.getProperty("jdbc.default.driverClassName");
            String driverUrl = prop.getProperty("jdbc.default.url");
            String dbUsername = prop.getProperty("jdbc.default.username");
            String dbPassword = prop.getProperty("jdbc.default.password");
            logger.info("JDBC URL IS " + driverUrl);
            if (driverClassName.contains("mysql")) {
                dialect = "org.hibernate.dialect.MySQL5Dialect";
            }
            if (driverClassName.contains("jtds")) {
                dialect = "org.hibernate.dialect.SQLServerDialect";
            }
            if (driverClassName.contains("mssql")) {
                dialect = "org.hibernate.dialect.SQLServerDialect";
            }
            if (driverClassName.contains("microsoft")) {
                dialect = "org.hibernate.dialect.SQLServerDialect";
            }
            if (driverClassName.contains("oracle")) {
                dialect = "org.hibernate.dialect.Oracle9Dialect";
            }
            org.orm.cfg.JDBCConnectionSetting jdbcConnectionSetting = new org.orm.cfg.JDBCConnectionSetting();
            jdbcConnectionSetting.setDialect(dialect);
            jdbcConnectionSetting.setConnectionURL(driverUrl);
            jdbcConnectionSetting.setDriverClass(driverClassName);
            jdbcConnectionSetting.setUserName(dbUsername);
            jdbcConnectionSetting.setPassword(dbPassword);
            HcerPersistentManager.setJDBCConnectionSetting(jdbcConnectionSetting);
        } catch (Exception e) {
            logger.error("#### HCER DATABASE CONFIG NOT CORRECT. PLS CONFIGURE YOUR HCER.PROPERTIES FILE ####");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    logger.error(ExceptionUtils.getStackTrace(e));
                }
            }
        }

    }

    public void contextDestroyed(ServletContextEvent sce) {

        this.context = null;
    }

    public static ITransformationService getTransformationService() {
        return tService;
    }

    public static ClientConnectorConsumer getClientConnectorConsumer() {
        return clientConectorConsumer;
    }

    public static String getRunningMode() {
        return runningMode;
    }

    public static void setRunningMode(String runningMode) {
        MyServletContextListener.runningMode = runningMode;
    }

}
